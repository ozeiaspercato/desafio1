FROM nginx

LABEL MAINTAINER="Ozeias Correia <ozeiaspercato@gmail.com>" APP_NAME="DESAFIO"
LABEL APP_VERSION="1.0.0"

RUN apt-get update && apt-get install -y git nano curl git

EXPOSE 80
WORKDIR /usr/share/nginx/html
RUN curl https://gitlab.com/ozeiaspercato/desafio1/-/raw/main/index.html?inline=false > index.html
